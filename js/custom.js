$(document).ready(function () {
    html = '<div class="message-body from-server typing-delay col-md-4"><span class="typing"></span><span class="typing"></span><span class="typing"></span></div>';
    $('#message-body').append(html);
    var url = 'http://localhost:3000/api/session';
    $.ajax(url, {
        'type': 'POST',
        'processData': false,
        'contentType': 'application/json'
    }).done(function (data) {
        var inTenMinutes = new Date(new Date().getTime() + 10 * 60 * 1000);
        Cookies.set('server_session_id', data.session_id, {
            expires: inTenMinutes
        });
        $.ajax('http://localhost:3000/api/message', {
            'data': JSON.stringify({
                input: "",
                sessionId: data.session_id
            }),
            'type': 'POST',
            'processData': false,
            'contentType': 'application/json'
        }).done(function (data) {
            console.log(data);
            $('#message-body').find('.message-body.from-server.typing-delay').remove();
            createChatScreen(data);
        }).fail(function (err) {
            console.log(err);
        });
    }).fail(function (err) {
        console.log(err);
    });
})

function timer(ms) {
    return new Promise(res => setTimeout(res, ms));
}

async function createChatScreen(result) {
    if (result.output.generic) {
        var messages = result.output.generic;
        for (let i = 0; i < messages.length; i++) {
            const response_type = messages[i].response_type;
            const text = messages[i].text;
            var html = "";
            if (response_type == "text") {
                html = '<div class="message-body from-server col-9 col-md-9"><p>' + text + '</p></div>';
                $('#message-body').append(html);
                $('#user-message-input').prop("disabled", false);
                $('#user-message-input').attr("placeholder", "Type Here...");
                document.getElementById('user-message-input').focus();
            }
            if (response_type == "pause") {
                html = '<div class="message-body from-server typing-delay col-md-4"><span class="typing"></span><span class="typing"></span><span class="typing"></span></div>';
                $('#message-body').append(html);
                var pause_time = messages[i].time;
                $('#user-message-input').prop("disabled", false);
                $('#user-message-input').attr("placeholder", "Typing...");
                await timer(pause_time);
                $('#message-body').find('.message-body.from-server.typing-delay').remove();
            }
            if (response_type == "image") {
                var image_description = messages[i].description;
                var title = messages[i].title;
                var image = messages[i].source;
                var is_title = "";
                var is_description = "";
                if (title == "") {
                    is_title = 'd-none';
                }
                if (image_description == "") {
                    is_description = 'd-none';
                }
                html = '<div class="message-body from-server image-out col-9 col-md-9"><div class="image-wrapper"><img src="' + image + '" class="w-100" alt=""><p></p></div><div class="image-info-wrapper"><h3 class="image-title ' + is_title + '">' + title + '</h3><p class="img-description ' + is_description + '">' + image_description + '</p></div></div>';
                $('#message-body').append(html);
                $('#user-message-input').prop("disabled", false);
                $('#user-message-input').attr("placeholder", "Type Here...");
            }
            if (response_type == "option") {
                var options = messages[i].options;
                html = '<div class="options-group">';
                for (let j = 0; j < options.length; j++) {
                    const label = options[j].label;
                    const value = options[j].value.input.text;
                    html += '<a href="javascript:void(0);" rel="' + value + '" class="option-selector d-block">' + label + '</a>';
                }
                html += '</div>';
                $('.action-bar').append(html);
                $('#user-message-input').prop("disabled", true);
                $('#user-message-input').attr("placeholder", "Select Option");
            }
            $('.chat-screen').animate({ scrollTop: $('.chat-screen').prop("scrollHeight") });
        }
    }
    if (result.output.user_defined && result.output.user_defined.custom.show_calendar == true) {
        var calendar_html = '<div class="options-group datepicker-option"> <div style="overflow:hidden;"> <div class="form-group"> <div class="row"> <div class="col-md-12"> <input id="datepicker-select" type="hidden" name="date" value=""> </div> </div> </div> </div> </div>';
        $('.action-bar').append(calendar_html);
        $('#user-message-input').prop("disabled", true);
        $('#user-message-input').attr("placeholder", "Select Date and Time");
        $('#datepicker-select').datetimepicker({
            inline: true,
            sideBySide: false,
            format: 'MMMM Do YYYY, h:mm a'
        })
        $('#datepicker-select').on('change.datetimepicker', function (e) {
            $('#user-message-input').val(moment(e.date._d).format('MMMM Do YYYY, h:mm a'));
        });
        var action_bar_height = $('.action-bar').height() + 30 + "px";
        $("#message-body").css({ "margin-bottom": action_bar_height });
        $('.chat-screen').animate({ scrollTop: $('.chat-screen').prop("scrollHeight") });
    }
}

function sendMessage(session_id, message) {
    $.ajax('http://localhost:3000/api/message', {
        'data': JSON.stringify({
            input: message,
            sessionId: session_id
        }),
        'type': 'POST',
        'processData': false,
        'contentType': 'application/json'
    }).done(function (data) {
        console.log(data);
        createChatScreen(data);
    }).fail(function (err) {
        console.log(err);
    });
}

$('.send-icon').click(function (e) {
    e.preventDefault();
    sessionChecker();
    widgetChecker();
    var user_session_id = Cookies.get('server_session_id');
    var input = $('#user-message-input').val();
    if (input == "") { return; }
    $('#user-message-input').val("");
    var html = '<div class="message-body from-user col-6 col-md-9"><p>' + input + '</p></div>';
    $('#message-body').append(html);
    $('.chat-screen').animate({ scrollTop: $('.chat-screen').prop("scrollHeight") });
    $('#user-message-input').prop("disabled", true);
    $('#user-message-input').attr("placeholder", "Please wait...");
    sendMessage(user_session_id, input);
});

$("#user-message-input").keyup(function (event) {
    if (event.keyCode === 13) {
        $(".send-icon").click();
    }
});

$(".action-bar").on('click', '.option-selector', function (e) {
    e.preventDefault();
    sessionChecker();
    var value = $(this).attr("rel");
    var label = $(this).text();
    var html = '<div class="message-body from-user col-6 col-md-9"><p>' + label + '</p></div>';
    $('#message-body').append(html);
    $('.action-bar').find('.options-group').remove();
    $('.chat-screen').animate({ scrollTop: $('.chat-screen').prop("scrollHeight") });
    $('#user-message-input').prop("disabled", true);
    $('#user-message-input').attr("placeholder", "Please wait...");
    var user_session_id = Cookies.get('server_session_id');
    sendMessage(user_session_id, value);
})

function sessionChecker() {
    var session = Cookies.get('server_session_id');
    if (!session) {
        location.reload();
    }
}

function widgetChecker() {
    var datepicker = $('.action-bar').find(".options-group.datepicker-option");
    if (datepicker.length > 0) {
        $('.action-bar').find(".options-group.datepicker-option").remove();
        $("#message-body").css({ "margin-bottom": "60px" });
        $('.chat-screen').animate({ scrollTop: $('.chat-screen').prop("scrollHeight") });
    }
}